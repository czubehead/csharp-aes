csharp-aes [abandoned]
==========

*This project is abandoned, as I no longer have use for it. As far as I know there are no security implications, 
it just means that there won't be any future updates. You are welcome to fork this and maintain it further.*

----------------

AES encryption for C\# using 256-bit Rijndael algorithm. You can encrypt using both `byte[]` key and a string password.

*AES stands for advanced encryption standard and nowadays is considered uncrackable.*

Key features
------------

-   Key- and **string-based encryption**
-   Ciphertext authentication (**HMAC**)
-   Plaintext authentication
-   Constant-time array comparison for critical parts as a **protection against timing attacks**
-   **Object encryption** based on XML serialization
-   On-stream encryption (encrypt a lot of data without loading it all into memory)
-   **No dependencies**

Installation
------------

Open Package Manager Console (in Visual Studio) and type:

    PM> Install-Package csharp-aes

Samples
-------

### Encrypt a string with a string password

``` csharp
const string password = "42";
string plaintext = "Gravity is a myth, Earth sucks!";
string cipher;
using (var aes = new PasswordAes(password))
    cipher = aes.Encrypt(plaintext);
```

And decrypt

``` csharp
using (var aes = new PasswordAes(password))
    Console.WriteLine(aes.Decrypt(cipher));

//output: Gravity is a myth, Earth sucks!
```

### Encrypt an object using a string password

First, our class:

``` csharp
public class Person
{
    public string Name { get; set; }

    [XmlIgnore]
    public int Age { get; set; }
}
```

then the code:

``` csharp
Person toEncrypt = new Person { Name = "Lenka", Age = 18 }, decrypted;
string password = "42";

string cipher;

using (var aes = new PasswordAes(password))
    cipher = aes.Encrypt(toEncrypt);
```

decrypt:

``` csharp
using (var aes = new PasswordAes(password))
{
    decrypted = aes.Decrypt<Person>(cipher);
    Console.WriteLine($"{decrypted.Name} is {decrypted.Age} yrs old");
}

Console.WriteLine($"{decrypted.Name} is {decrypted.Age} yrs old");
//output: Lenka is 0 yrs old
```

XmlSerializer is used to serialize objects, which means that when `XmlIgnore` was declared on `Person.Age` it was not saved. You may tweak the class this way.

*Note that both `SimpleAes` and `PasswordAes` are supposed to be used in `using` blocks*

### Encrypt a string using byte\[\] key

The key must be 256 bits long (32 bytes). In this example the key is generated automatically.

``` csharp
byte[] key;
var plaintext = "Why so serious?";
string cipher;
using (var aes = new AdvancedAes())
{
    //key is generated automatically. You may also pass 
    //it as constructor parameter to use your own
    key = aes.EncryptionKey;
    cipher = aes.Encrypt(plaintext);
}
```

decryption:

``` csharp
using (var aes = new AdvancedAes(key))
    Console.WriteLine(aes.Decrypt(cipher));
    
//output: Why so serious?
```

### Files

You want to use [streams](https://gitlab.com/czubehead/csharp-aes/wikis/streams) for files.

------------------------------------------------------------------------

Security notes
--------------

1.  This library does **not** use encrypted memory and thus is to be run in a trusted environment. This is because `Rfc2898DeriveBytes` supports `string` input only and streams are used internally for everything. However, security of the output is not compromised given no bad guys had access to the program's memory.
2.  Do **not** encrypt passwords, look up a recent way of storing passwords instead. Typically hashing and salting is used.

Links
-----

-   [NuGet package](https://www.nuget.org/packages/csharp-aes/)
-   [Author's personal website](https://petrcech.eu)
-   [Donate](https://paypal.me/czubehead)
-   Donate BTC: [1P37itb28STm8vLB6gte4ydfMZq4NgaDBo](bitcoin:1P37itb28STm8vLB6gte4ydfMZq4NgaDBo)

