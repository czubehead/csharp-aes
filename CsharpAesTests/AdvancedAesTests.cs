﻿using System;
using System.IO;
using System.Text;
using CsEncryptionTests;
using CsharpAes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable RedundantAssignment
// ReSharper disable UnusedVariable

namespace CsharpAesTests
{
    [TestClass]
    public class AdvancedAesTests
    {
        /// <summary>
        ///     Tests for valid and invalid key sizes
        /// </summary>
        [TestMethod]
        public void KeySizeTest()
        {
            #region invalid

            byte[][] invalidKeys =
            {
                new byte[125],
                new byte[64],
                new byte[512]
            };
            // ReSharper disable once NotAccessedVariable
            AdvancedAes aes;

            foreach (var invalidKey in invalidKeys)
                try
                {
                    aes = new AdvancedAes(invalidKey);
                    Assert.Fail();
                }
                catch (ArgumentException)
                {
                }

            #endregion

            #region valid

            var validKey = new byte[256 / 8];

            try
            {
                aes = new AdvancedAes(validKey);
            }
            catch
            {
                Assert.Fail();
            }

            #endregion
        }

        /// <summary>
        ///     Test string encryption
        /// </summary>
        [TestMethod]
        public void EncryptTest()
        {
            const string toEncrypt = "Why so serious?";
            var key = new byte[256 / 8];
            var encoding = Encoding.UTF8;
            var cipherStream = new MemoryStream();
            using (var source = new MemoryStream(encoding.GetBytes(toEncrypt)))
            {
                using (var aes = new AdvancedAes(key))
                {
                    aes.Encrypt(source, cipherStream, false);
                }
            }
            cipherStream.Flush();
            cipherStream.Position = 0;

            using (var dest = new MemoryStream())
            {
                using (var aes = new AdvancedAes(key))
                {
                    aes.Decrypt(cipherStream, dest, false);
                    dest.Flush();
                    var s = encoding.GetString(dest.ToArray());

                    Assert.AreEqual(toEncrypt, s);
                }
            }
        }

        /// <summary>
        ///     Tests dummy class encryption
        /// </summary>
        [TestMethod]
        public void EncryptObjectTest()
        {
            var u = new DummyUser {Name = "Jon Snow"};
            string cipher;
            byte[] key;

            using (var aes = new AdvancedAes())
            {
                key = aes.EncryptionKey;
                cipher = aes.Encrypt(u);
            }

            using (var aes = new AdvancedAes(key))
            {
                Assert.AreEqual(u.Name, aes.Decrypt<DummyUser>(cipher).Name);
            }
        }

        /// <summary>
        ///     Tests for wrong keys
        /// </summary>
        [ExpectedException(typeof(IncorrectPasswordException))]
        [TestMethod]
        public void WrongKeyTest()
        {
            const string toEncrypt = "The cake is a lie";
            byte[] key1 = new byte[256 / 8],
                key2 = new byte[256 / 8];

            key2[0] = 1;

            string cipher;
            using (var aes = new AdvancedAes(key1))
            {
                cipher = aes.Encrypt(toEncrypt);
            }

            using (var aes = new AdvancedAes(key2))
            {
                var decrypted = aes.Decrypt(cipher);
                Assert.Fail();
            }
        }

        /// <summary>
        ///     Throws random rubbish at Decrypt
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void FormatTest()
        {
            var aes = new AdvancedAes();

            aes.Decrypt(
                $"{GenerateText(4)}${GenerateText(16)}${GenerateText(64)}${GenerateText(64)}${GenerateText(1000)}${GenerateText(20)}");
        }

        /// <summary>
        ///     Tests disposed exceptions
        /// </summary>
        [TestMethod]
        public void DisposeTest()
        {
            var aes = new AdvancedAes();
            using (aes)
            {
                aes.AlgorithmVersion = 1;
            }

            aes.Dispose();
            aes.Dispose();
        }

        /// <summary>
        ///     Tests for non-ansii characters
        /// </summary>
        [TestMethod]
        public void SpecialCharsTest()
        {
            const string special = @"příliš žluťoučký kůň úpěl ďábelské ódy";

            byte[] key;
            string cipher;
            using (var aes = new AdvancedAes())
            {
                key = aes.EncryptionKey;
                cipher = aes.Encrypt(special);
            }

            using (var aes = new AdvancedAes(key))
            {
                var decrypted = aes.Decrypt(cipher);

                Assert.AreEqual(special, decrypted);
            }
        }

        /// <summary>
        ///     Tests if zero-length plaintext throws exceptions. They should NOT
        /// </summary>
        [TestMethod]
        public void EmptyInputTest()
        {
            const string plainText = "";
            var encoding = Encoding.UTF8;

            var aes = new AdvancedAes();
            try
            {
                var cipherText = aes.Encrypt(plainText);
                var cipherObj = aes.Encrypt<string>(plainText);

                var plainStream = new MemoryStream(new byte[0]);
                var cipherStream = new MemoryStream();
                aes.Encrypt(plainStream, cipherStream, false);
                cipherStream.Position = 0;
                plainStream.SetLength(0);

                aes.Decrypt(cipherText);
                aes.Decrypt<string>(cipherObj);
                aes.Decrypt(cipherStream, plainStream);

                Assert.IsTrue(true);
            }
            catch (ArgumentNullException)
            {
                Assert.Fail("Failed to take the input");
            }
            finally
            {
                aes.Dispose();
            }
        }

        private static string GenerateText(int length)
        {
            var from = "abcdefghijklmnopqrstuvwxyz";
            from += from.ToUpper();
            from += "0123456789+-";

            var sb = new StringBuilder();
            var ran = new Random();
            for (var i = 0; i < length; i++)
                sb.Append(from[ran.Next(0, from.Length - 1)]);

            return sb.ToString();
        }
    }
}