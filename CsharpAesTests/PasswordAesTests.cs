﻿using System;
using CsharpAes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable NotAccessedVariable

namespace CsharpAesTests
{
    [TestClass]
    public class PasswordAesTests
    {
        /// <summary>
        /// Tests validity of password
        /// </summary>
        [TestMethod]
        public void PasswordAesTest()
        {
            PasswordAes aes;
            try
            {
                aes = new PasswordAes(null);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
                //ignored
            }

            const string right = "42";
            try
            {
                aes = new PasswordAes(right);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Checks encryption/decryption
        /// </summary>
        [TestMethod]
        public void EncryptTest()
        {
            const string toEncrypt = "Why so serious?";
            const string password = "42";
            string cipher;

            using (var aes = new PasswordAes(password))
                cipher = aes.Encrypt(toEncrypt);

            try
            {
                string plain;
                using (var aes = new PasswordAes(password))
                    plain = aes.Decrypt(cipher);
                Assert.AreEqual(toEncrypt, plain);
            }
            catch (IncorrectPasswordException)
            {
                Assert.Fail("bad password");
            }
        }
    }
}