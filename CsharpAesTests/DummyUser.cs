﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsEncryptionTests
{
    /// <summary>
    /// Dummy class for testing purposes
    /// </summary>
    public class DummyUser
    {
        public string Name { get; set; }

        public override string ToString()
            => Name;
    }
}
