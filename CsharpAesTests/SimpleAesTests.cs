﻿using System;
using System.IO;
using System.Text;
using CsharpAes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsharpAesTests
{
    [TestClass]
    public class SimpleAesTests
    {
        /// <summary>
        /// Tests for valid and invalid key sizes
        /// </summary>
        [TestMethod]
        public void KeySizeTest()
        {
            #region invalid

            byte[][] invalidKeys =
            {
                new byte[125],
                new byte[64],
                new byte[512],
            };
            // ReSharper disable once NotAccessedVariable
            SimpleAes aes;

            foreach (var invalidKey in invalidKeys)
            {
                try
                {
                    aes = new SimpleAes(invalidKey);
                    Assert.Fail();
                }
                catch (ArgumentException)
                {

                }
            }

            #endregion

            #region valid

            byte[][] validKeys =
            {
                new byte[128/8],
                new byte[192/8],
                new byte[256/8],
            };

            foreach (var validKey in validKeys)
            {
                try
                {
                    aes = new SimpleAes(validKey);
                }
                catch
                {
                    Assert.Fail();
                }
            }
            #endregion
        }

        /// <summary>
        /// Tests encryption / decryption
        /// </summary>
        [TestMethod]
        public void EncryptDecryptTest()
        {
            byte[] key;
            byte[] iv;
            byte[] cipher;
            const string toDecrypt = "Gravity is a myth, Earth sucks";

            using (SimpleAes aes = new SimpleAes())
            {
                key = aes.Key;
                iv = aes.IV;
                cipher = aes.Encrypt(toDecrypt);
            }

            using (SimpleAes aes = new SimpleAes(key, iv))
            {
                Assert.AreEqual(toDecrypt, aes.Decrypt(cipher));
            }
        }

        [TestMethod]
        public void StreamTest()
        {
            byte[] key;
            byte[] iv;
            MemoryStream cipher = new MemoryStream();
            const string plainText = "Earth sucks";
            
            using (SimpleAes aes = new SimpleAes())
            {
                key = aes.Key;
                iv = aes.IV;
                MemoryStream source = new MemoryStream(Encoding.UTF8.GetBytes(plainText));

                aes.Encrypt(source, cipher, false);
            }
            cipher.Position = 0;

            using (SimpleAes aes = new SimpleAes(key, iv))
            {
                MemoryStream plain = new MemoryStream();

                aes.Decrypt(cipher, plain);

                Assert.AreEqual(plainText, Encoding.UTF8.GetString(plain.ToArray()));

            }
        }
    }
}